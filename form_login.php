<?php
  session_start();

  if(isset($_SESSION['username']) && $_SESSION['username'] != '')
  {
    // header("location:".$_SERVER["HTTP_REFERER"]);
    header("location:form_login.php");
  }
?>

<!DOCTYPE html>

<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Login Form</title>
  <link rel="stylesheet" href="css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <?php
    if(isset($_GET['success']) && !empty($_GET['success']))
    {
      ?>
        <script type="text/javascript">
            window.alert("Selamat Pendaftaran anda berhasil silahkan login !");
        </script>
      <?php
    }
    elseif(isset($_GET['error']) && !empty($_GET['error']))
    {
      ?>
        <script type="text/javascript">
            window.alert("USERNAME ATAU PASSWORD SALAH !");
        </script>
      <?php
    }
  ?>
  <section class="container">
    <div class="login">
      <h1>Login </h1>
      <form method="post" action="login_proses.php">
        <p><input type="text" name="username" value="" placeholder="Username" required=""></p>
        <p><input type="password" name="password" value="" placeholder="Password" required=""></p>
        
        <p class="submit"><input type="submit" name="commit" value="Login"></p>
      </form>
    </div>
    <div class="login-help">
      <p>Belum Punya Akun? <a href="add.php">Daftar</a>.</p>
    </div>
  </section>

</body>
</html>