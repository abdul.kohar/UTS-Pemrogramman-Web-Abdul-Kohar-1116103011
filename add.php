<?php
  if(isset($_GET['error']) && !empty($_GET['error']))
    {
      ?>
        <script type="text/javascript">
            window.alert("Password tidak sama !");
        </script>
      <?php
    }
?>

<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;    
}
</style>
</head>
<body>
<form method="post" action="add_proses.php">
<table style="width:70%">
  <tr>
    <td style="width:50%">Nama Pengguna</td>
    <td><input type="text" name="username" size="30"></td>
  </tr>
  <tr>
    <td>Password</td>
    <td><input type="password" name='password' size="30"></td>
  </tr>
  <tr>
    <td>Re-Type Password</td>
    <td><input type="password" name="password_re" size="30"></td>
  </tr>
  <tr>
    <td>E-mail</td>
    <td><input type="email" name="email" size="50"></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td><input type="text" name="nama" size="30"></td>
  </tr>
  <tr>
    <td>NIM</td>
    <td><input type="text" name="nim" size="30"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td><input type="text" name="alamat" size="30"></td>
  </tr>
  <tr>
    <td>Kota Asal</td>
    <td>
      <select name="kota">
        <option value="Bandung">Bandung</option>
        <option value="Malang">Malang</option>
        <option value="Surabaya">Surabaya</option>
        <option value="Medan">Medan</option>
      </select>
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
     <td><input type="radio" value='L' name="jk">Laki - Laki <input value="P" type="radio" name="jk">Perempuan</td>
  </tr>
  <tr>
    <td>Hobi</td>
    <td>
      <input type="checkbox" name="hobi" value="hiking"> Hiking <br>
      <input type="checkbox" name="hobi" value="traveling"> Traveling <br>
      <input type="checkbox" name="hobi" value="mancing"> Memancing <br>
      <input type="checkbox" name="hobi" value="koding"> Coding <br>
      
      
    </td>
  </tr>
  <tr>
    <td>Deskripsi Pribadi
    </td>
    <td><textarea cols="50" name="deskripsi" rows="15">Deskripsi Anda</textarea></td>
  </tr>
  
</table>
	<input type="submit" name="submit" value="Add"><input type="reset" value="hapus">
</form>
</body>
</html>

</body>
</html>