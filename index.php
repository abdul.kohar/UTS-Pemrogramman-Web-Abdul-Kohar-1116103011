<?php
	session_start();

	if(!isset($_SESSION['username']) && $_SESSION['username'] == '')
	{
		// header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:form_login.php");
	}
	include('koneksi.php');

	$data = mysqli_query($conn," SELECT * FROM users");

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even){background-color: #f2f2f2}

            th {
                background-color: #4CAF50;
                color: white;
            }
         </style>
</head>
<body>

	<a href='add.php'> + Tambah Data </a>
	<a href='logout.php'> <font align="right">Logout</font> </a>
	<table>
		<tr>
			<th>ID</th>
			<th>Email</th>
			<th>Nama</th>
			<th>Username</th>
			<th>Opsi</th>
		</tr>
		<?php
			$no = 1;

			while ($res = mysqli_fetch_array($data)) {

				echo "
				<tr>
					<td>$no</td>
					<td>$res[email]</td>
					<td>$res[nama]</td>
					<td>$res[username]</td>
					<td>
						<a href='edit.php?id=$res[id]'>Edit</a> || 
						<a href=\"delete_proses.php?id=$res[id]\" onClick=\"return confirm('Apakah Anda Yakin Ingin Menghapus?')\">Hapus</a>
						
					</td>
				</tr>";	

				$no++;
				// ini adalah komen
			}
		?>
	</table>
</body>
</html>

