-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2017 at 04:27 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas1`
--
CREATE DATABASE IF NOT EXISTS `tugas1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tugas1`;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `nim` int(10) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(15) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `hobi` varchar(20) NOT NULL,
  `deskripsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `nama`, `nim`, `alamat`, `kota`, `jk`, `hobi`, `deskripsi`) VALUES
(1, 'abdul.kohar', 'd41d8cd98f00b204e9800998ecf8427e', 'abdul@app.com', 'Abdul Kohar', 1116103011, 'Nanjung', 'Bandung', 'L', '', '  Ganteng'),
(2, 'abdul.kohar', 'c7e7fb67ffb2cc3c1857b78131e01a6b', 'ak.kohar@app.com', '', 0, '', 'Bandung', 'P', 'traveling', 'Deskripsi Anda'),
(4, 'abdul.kohar', '81dc9bdb52d04dc20036dbd8313ed055', 'ak.kohar@app.com', '', 0, '', 'Bandung', 'P', 'traveling', 'Deskripsi Anda'),
(6, 'abdul.kohar', '7815696ecbf1c96e6894b779456d330e', 'admin@app.com', 'Randy', 1116103011, 'Nanjung', 'Malang', 'P', 'hiking', 'Deskripsi Anda');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
