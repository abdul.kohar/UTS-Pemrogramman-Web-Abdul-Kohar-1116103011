<?php





class insert{
	public $table = '';
	public $column = [];
	public $values = [];

	public function lakukan($conn){

		$query = "INSERT INTO ".$this->table." (";
		$coma_cond = 0;
		foreach ($this->column as  $column) {
			$coma = $coma_cond == 0 ? "" : ",";
			$query .= "".$coma.$column;
			$coma_cond = 1;
		}

		$query .= ") VALUES (";
		$coma_cond = 0;
		foreach ($this->values as  $value) {
			$coma = $coma_cond == 0 ? "" : ",";
			$query .= "".$coma."\"$value\"";
			$coma_cond = 1;
		}

		$query .= ")";

		$result = mysqli_query($conn, $query);

		if($result)
		  	return true;
		 else
		 	return false;
	}
}

class update{
	public $table = '';
	public $params = [];
	public $conditions = [];

	public function lakukan($conn){

		$query = "UPDATE ".$this->table." SET ";
		$coma_cond = 0;

		foreach ($this->params as $key => $param) {
			$coma = $coma_cond == 0 ? "" : ",";
			
			$query .= "".$coma.$key." = \"$param\" ";
			
			$coma_cond = 1;
		}

		$query .= "where";
		$coma_cond = 0;
		foreach ($this->conditions as  $key => $condition) {
			$coma = $coma_cond == 0 ? "" : "AND";
			$query .= "".$coma." ".$key." = \"$condition\"";
			$coma_cond = 1;
		}

		$result = mysqli_query($conn, $query);

		if($result)
		  	return true;
		 else
		 	return false;
	}
}

class delete{
	public $table = '';
	public $conditions = [];

	public function lakukan($conn){

		$query = "DELETE FROM ".$this->table." WHERE ";

		$coma_cond = 0;
		foreach ($this->conditions as  $key => $condition) {
			$coma = $coma_cond == 0 ? "" : "AND";
			$query .= "".$coma." ".$key." = \"$condition\"";
			$coma_cond = 1;
		}
		return $query;
		$result = mysqli_query($conn, $query);

		if($result)
		  	return true;
		 else
		 	return false;
	}
}

?>