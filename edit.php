<?php

  session_start();

  if(!isset($_SESSION['username']) && $_SESSION['username'] == '')
  {
    // header("location:".$_SERVER["HTTP_REFERER"]);
    header("location:form_login.php");
  }
  include('koneksi.php');
  $id = $_GET['id'];
  $data = mysqli_query($conn," SELECT * FROM users where id = ".$id);

  $res = mysqli_fetch_array($data);

?>

<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;    
}
</style>
</head>
<body>
<form method="post" action="edit_proses.php">
<table style="width:70%">
  <input type="hidden" name="id" value="<?=$id?>">
  <tr>
    <td style="width:50%">User Name</td>
    <td><input type="text" name="username" <?= !empty($res['username']) ? "value='$res[username]'" : '' ?> size="30"></td>
  </tr>
  <tr>
    <td>Password</td>
    <td><input type="password" name='password' size="30"></td>
  </tr>
  <tr>
    <td>Re-Type Password</td>
    <td><input type="password" name="password" size="30"></td>
  </tr>
  <tr>
    <td>E-mail</td>
    <td><input type="email" name="email"  size="50" <?= !empty($res['email']) ? "value='$res[email]'" : '' ?>></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td><input type="text" name="nama" size="30" <?= !empty($res['nama']) ? "value='$res[nama]'" : '' ?>></td>
  </tr>
  <tr>
    <td>NIM</td>
    <td><input type="text" name="nim" size="30" <?= !empty($res['nim']) ? "value='$res[nim]'" : '' ?>></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td><input type="text" name="alamat" size="30" <?= !empty($res['alamat']) ? "value='$res[alamat]'" : '' ?>></td>
  </tr>
  <tr>
    <td>Kota Asal</td>
    <td>
      <select name="kota">
        <option value="Bandung" <?= !empty($res['kota']) && $res['kota'] == 'Bandung' ? "checked" : '' ?>>Bandung</option>
        <option value="Malang" <?= !empty($res['kota']) && $res['kota'] == 'Malang' ? "checked" : '' ?> >Malang</option>
        <option value="Surabaya" <?= !empty($res['kota']) && $res['kota'] == 'Surabaya' ? "checked" : '' ?>>Surabaya</option>
        <option value="Medan" <?= !empty($res['kota']) && $res['kota'] == 'Medan' ? "checked" : '' ?>>Medan</option>
      </select>
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
     <td><input type="radio" value='L' name="jk" <?= !empty($res['jk'])  && $res['jk']== 'L' ? "checked" : '' ?>>Laki - Laki <input value="P" type="radio" name="jk" <?= !empty($res['jk'])  && $res['jk'] == 'P' ? "checked" : '' ?>>Perempuan</td>
  </tr>
  <tr>
    <td>Hobi</td>
    <td>
      <input type="checkbox" name="hobi" value="hiking" <?= !empty($res['hobi'])  && $res['hobi']== 'hiking' ? "checked" : '' ?>> Hiking <br>
      <input type="checkbox" name="hobi" value="traveling"  <?= !empty($res['hobi'])  && $res['hobi']== 'traveling' ? "checked" : '' ?>> Traveling <br>
      <input type="checkbox" name="hobi" value="mancing"  <?= !empty($res['hobi'])  && $res['hobi']== 'mancing' ? "checked" : '' ?>> Memancing <br>
      <input type="checkbox" name="hobi" value="koding"  <?= !empty($res['hobi'])  && $res['hobi']== 'koding' ? "checked" : '' ?>> Coding <br>
      
      
    </td>
  </tr>
  <tr>
    <td>Deskripsi Pribadi
    </td>
    <td><textarea cols="50" name="deskripsi" rows="15"> <?= !empty($res['deskripsi'])  ? "$res[deskripsi]" : '' ?></textarea></td>
  </tr>
  
</table>
	<input type="submit" name="submit" value="Update"><input type="reset" value="hapus">
</form>
</body>
</html>

</body>
</html>